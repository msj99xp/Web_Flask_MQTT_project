from flask import Flask, render_template,request, jsonify
import database as db

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('testt.html')
 

@app.route("/_button")
def _button():
    alldata=db.read_heartdata()
    state=alldata[3]
    alldata=db.read_tempdata()
    state2=alldata[3]
    alldata=db.read_btempdata()
    state3=alldata[3]
    alldata=db.read_humiditydata()
    state4=alldata[3]
    alldata=db.read_bodydata()
    state5=alldata[3]
    state6=alldata[5]
    state7=alldata[7]
    allstate=str(state)+","+str(state2)+","+str(state3)+","+str(state4)+","+str(state5)+","+str(state6)+","+str(state7)
    return jsonify(buttonState=allstate)
    


#@app.route("/_bodytemperature")
#def _bodytemperature():
#    alldata=db.read_btempdata()
#    state=alldata[3]
#    return jsonify(bodytemperaturestate=state)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
