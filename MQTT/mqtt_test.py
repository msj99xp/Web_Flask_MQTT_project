import paho.mqtt.client as mqtt
import string
import database as db
import smtplib
from threading import Timer
import time

alert_flag=0

user_name=""
user_password=""
send_mail=["",""]

MQTT_TOPIC1 = "smart/zaim/temp"
MQTT_TOPIC2 = "smart/zaim/humid"
MQTT_TOPIC3 = "smart/zaim/vibe"
MQTT_TOPIC4 = "smart/zaim/soil"

def timeout():
	global alert_flag
	print ("Timeout")
	alert_flag=0

def _send_email(user_email,user_password,send_email,msg):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    # server.login("zaimsky@gmail.com", "zaim885521")
    server.login(user_email, user_password)
 
    print(msg)
    # server.sendmail("zaimsky@gmail.com", send_email, msg)
    server.sendmail(user_email, send_email, msg)
    server.quit()



# The callback for when the client receives a CONNACK response from the server.
def on_connect(self, mosq, obj, rc):
    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    # client1.subscribe("armtronix_mqtt")
    client1.subscribe(MQTT_TOPIC1, 0)
    client1.subscribe(MQTT_TOPIC2, 0)
    client1.subscribe(MQTT_TOPIC3, 0) 
    client1.subscribe(MQTT_TOPIC4, 0)    
    
def check_limit(cval,minval,maxval):
    if cval>=minval and cval<=maxval:
	    return False
    else:
        return True	 

def get_email():
    global send_mail
    data_send_email = db.read_send_email()
    send_mail[0]=data_send_email[0][1]
    send_mail[1]=data_send_email[1][1]	
    
# The callback for when a PUBLISH message is received from the server.
def on_message(mosq, obj, msg):
    global alert_flag
    global user_name
    global user_password
    global send_mail
    print(msg.topic+" "+str(msg.payload))
    tempdata=0
    tempmin=0
    tempmax=0
    email_to_send=False
    email_msg=""
    
    if(msg.topic==MQTT_TOPIC1):       
        db.insert_tempdata(msg.payload)
        tempmin = db.read_temp_limit_min()
        tempmax = db.read_temp_limit_max()
        email_msg= "Temperature Value Out of Limit= " +  str(msg.payload) 
        email_to_send=check_limit(int(msg.payload),tempmin[1],tempmax[1])
          
    if(msg.topic==MQTT_TOPIC4):       
        db.insert_soildata(msg.payload)
        tempmin = db.read_soil_limit_min()
        tempmax = db.read_soil_limit_max()
        email_msg= "Soil Moisture Value Out of Limit= " +  str(msg.payload)
        email_to_send=check_limit(int(msg.payload),tempmin[1],tempmax[1])
        
    if(msg.topic==MQTT_TOPIC2):       
        db.insert_humiditydata(msg.payload)
        tempmin = db.read_humid_limit_min()
        tempmax = db.read_humid_limit_max()
        email_msg= "Humidity Value Out of Limit= " +  str(msg.payload) 
        email_to_send=check_limit(int(msg.payload),tempmin[1],tempmax[1])
         
    if(msg.topic==MQTT_TOPIC3):       
        db.insert_vibrationdata(msg.payload) 
        tempmin = db.read_vibe_limit_min()
        tempmax = db.read_vibe_limit_max()
        email_msg= "Vibration Value Out of Limit= " +  str(msg.payload) 
        email_to_send=check_limit(int(msg.payload),tempmin[1],tempmax[1])
          
    if alert_flag == 0 and email_to_send==True:
        print("Send Email")
        get_email()
        _send_email(user_name,user_password,send_mail[0],email_msg)
        _send_email(user_name,user_password,send_mail[1],email_msg)
        alert_flag=1
        email_to_send=False
        t = Timer(20, timeout)
        t.start()    

client1 = mqtt.Client()
client1.on_connect = on_connect
client1.on_message = on_message

data_user_email = db.read_user_email()
user_name = data_user_email[1]
user_password = data_user_email[2]



client1.connect("192.168.0.165", 1883, 60)
# client1.connect("iot.eclipse.org", 1883, 60)


# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.

client1.loop_forever()
