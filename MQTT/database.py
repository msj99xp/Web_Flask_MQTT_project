#!/usr/bin/env python

import MySQLdb


def insert_tempdata(temp):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()   
    curs.execute("""insert into temp (timestamp,temp_value) values (NOW(), """+str(temp)+""") """)   
    db.commit()

def insert_soildata(soil):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()    
    curs.execute("""insert into soil (timestamp,soil_value) values (NOW(), """+str(soil)+""") """)   
    db.commit()

def insert_humiditydata(humid):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""insert into humid (timestamp,humid_value) values (NOW(), """+str(humid)+""") """)   
    db.commit()

def insert_vibrationdata(vibe):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""insert into vibe (timestamp,vibe_value) values (NOW(), """+str(vibe)+""") """)   
    db.commit()
 
    
def read_tempdata():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM temp where DATE(timestamp) = CURRENT_DATE() ORDER BY timestamp DESC")
    alldata=curs.fetchone()
    db.commit()
    return alldata

def read_soildata():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM soil where DATE(timestamp) = CURRENT_DATE() ORDER BY timestamp DESC")
    alldata=curs.fetchone()
    db.commit()
    return alldata

def read_vibedata():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM vibe where DATE(timestamp) = CURRENT_DATE() ORDER BY timestamp DESC")
    alldata=curs.fetchone()
    db.commit()
    return alldata

def read_humiddata():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM humid where DATE(timestamp) = CURRENT_DATE() ORDER BY timestamp DESC")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def update_temp_limit_min(temp):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update temp_limit_min set temp_val="""+str(temp))   
    db.commit()
    
def update_temp_limit_max(temp):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update temp_limit_max set temp_val="""+str(temp))   
    db.commit()

def update_vibe_limit_min(vibe):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update vibe_limit_min set vibe_val="""+str(vibe))   
    db.commit()

def update_vibe_limit_max(vibe):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update vibe_limit_max set vibe_val="""+str(vibe))   
    db.commit()

def update_soil_limit_min(soil):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update soil_limit_min set soil_val="""+str(soil))   
    db.commit()
    
def update_soil_limit_max(soil):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update soil_limit_max set soil_val="""+str(soil))   
    db.commit()
    
def update_humid_limit_min(humid):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update humid_limit_min set humid_val="""+str(humid))   
    db.commit()
    
def update_humid_limit_max(humid):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("""update humid_limit_max set humid_val="""+str(humid))   
    db.commit()
    
def read_temp_limit_min():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM temp_limit_min")
    alldata=curs.fetchone()
    db.commit()
    return alldata

def read_temp_limit_max():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM temp_limit_max")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_vibe_limit_min():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM vibe_limit_min")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_vibe_limit_max():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM vibe_limit_max")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_soil_limit_max():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM soil_limit_max")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_soil_limit_min():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM soil_limit_min")
    alldata=curs.fetchone()
    db.commit()
    return alldata
   
def read_humid_limit_min():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM humid_limit_min")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_humid_limit_max():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM humid_limit_max")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def read_temp_filter(start_date,end_date):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM temp WHERE timestamp BETWEEN \"" + start_date + "\" AND \"" + end_date + "\" ORDER BY timestamp DESC")
    alldata=curs.fetchall()
    db.commit()
    return alldata
    
def read_vibe_filter(start_date,end_date):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM vibe WHERE timestamp BETWEEN \"" + start_date + "\" AND \"" + end_date + "\" ORDER BY timestamp DESC")
    alldata=curs.fetchall()
    db.commit()
    return alldata
    
def read_soil_filter(start_date,end_date):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM soil WHERE timestamp BETWEEN \"" + start_date + "\" AND \"" + end_date + "\" ORDER BY timestamp DESC")
    alldata=curs.fetchall()
    db.commit()
    return alldata
    
def read_humid_filter(start_date,end_date):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM humid WHERE timestamp BETWEEN \"" + start_date + "\" AND \"" + end_date + "\" ORDER BY timestamp DESC")
    alldata=curs.fetchall()
    db.commit()
    return alldata
    
def read_send_email():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM send_email")
    alldata=curs.fetchall()
    db.commit()
    return alldata
    
def read_user_email():
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute ("SELECT * FROM user_email")
    alldata=curs.fetchone()
    db.commit()
    return alldata
    
def update_send_email(user_name):
    db = MySQLdb.connect("localhost", "root", "shark123", "landslide_monitor")
    curs=db.cursor()
    curs.execute("update send_email set user_name=\""+str(user_name)+"\" WHERE user_id=2")   
    db.commit()
   
    



