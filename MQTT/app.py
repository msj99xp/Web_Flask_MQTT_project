from flask import Flask, render_template, request, url_for, redirect, jsonify
import database as db


app = Flask(__name__)


curr_temp=0
skin_conds=0
humiditys=0
bodytemps=0
curr_vibe=0
curr_soil=0
temps=0

str_filter =''
@app.route('/')
def index():
    return render_template('All.html')
    
@app.route('/summary', methods=['GET', 'POST'])
def summary():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('summary.html')

@app.route('/temps', methods=['GET', 'POST'])
def temps():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('temps.html')
    
@app.route('/temp_limit_min', methods=['GET', 'POST'])
def temps_limit_min():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_temp_limit_min(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index')) 

@app.route('/temp_limit_max', methods=['GET', 'POST'])
def temps_limit_max():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_temp_limit_max(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))

@app.route('/vibe_limit_min', methods=['GET', 'POST'])
def vibes_limit_min():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_vibe_limit_min(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))

@app.route('/vibe_limit_max', methods=['GET', 'POST'])
def vibes_limit_max():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_vibe_limit_max(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))
    
@app.route('/soil_limit_min', methods=['GET', 'POST'])
def soils_limit_min():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_soil_limit_min(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))

@app.route('/soil_limit_max', methods=['GET', 'POST'])
def soils_limit_max():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_soil_limit_max(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))

@app.route('/humid_limit_min', methods=['GET', 'POST'])
def humids_limit_min():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_humid_limit_min(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))

@app.route('/humid_limit_max', methods=['GET', 'POST'])
def humids_limit_max():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_humid_limit_max(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))
    
@app.route('/send_email', methods=['GET', 'POST'])
def send_email():
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data) 
        db.update_send_email(new_data)
        return redirect(url_for('index')) 
         
    return redirect(url_for('index'))
    
@app.route('/temp_filter', methods=['GET', 'POST'])
def temp_filter():
    global str_filter
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data)
        a,b = new_data.split(",")
        tmpfilter=db.read_temp_filter(a,b)
        print (str(len(tmpfilter)))
        strall=str(len(tmpfilter))+"/"
        for i in range (len(tmpfilter)):
			strall=strall + "," + str (tmpfilter[i][2])+"="+ str(tmpfilter[i][1])			
        
        str_filter=strall       
        return redirect(url_for('index'))
         
    return redirect(url_for('index'))

@app.route('/vibe_filter', methods=['GET', 'POST'])    
def vibe_filter():
    global str_filter
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data)
        a,b = new_data.split(",")
        tmpfilter=db.read_vibe_filter(a,b)
        print (str(len(tmpfilter)))
        strall=str(len(tmpfilter))+"/"
        for i in range (len(tmpfilter)):
			strall=strall + "," + str (tmpfilter[i][2])+"="+ str(tmpfilter[i][1])			
        
        str_filter=strall       
        return redirect(url_for('index'))
         
    return redirect(url_for('index'))
    
@app.route('/humid_filter', methods=['GET', 'POST'])    
def humid_filter():
    global str_filter
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data)
        a,b = new_data.split(",")
        tmpfilter=db.read_humid_filter(a,b)
        print (str(len(tmpfilter)))
        strall=str(len(tmpfilter))+"/"
        for i in range (len(tmpfilter)):
			strall=strall + "," + str (tmpfilter[i][2])+"="+ str(tmpfilter[i][1])			
        
        str_filter=strall       
        return redirect(url_for('index'))
         
    return redirect(url_for('index'))

@app.route('/soil_filter', methods=['GET', 'POST'])    
def soil_filter():
    global str_filter
    if request.method == 'POST':
        new_data = request.get_data()
        print(new_data)
        a,b = new_data.split(",")
        tmpfilter=db.read_soil_filter(a,b)
        print (str(len(tmpfilter)))
        strall=str(len(tmpfilter))+"/"
        for i in range (len(tmpfilter)):
			strall=strall + "," + str (tmpfilter[i][2])+"="+ str(tmpfilter[i][1])			
        
        str_filter=strall       
        return redirect(url_for('index'))
         
    return redirect(url_for('index'))
               
@app.route('/skin_cond', methods=['GET', 'POST'])
def skin_cond():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('skin_cond.html')

@app.route('/humidity', methods=['GET', 'POST'])
def humidity():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('humidity.html')

@app.route('/vibe', methods=['GET', 'POST'])
def vibe():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('vibe.html')

@app.route('/soil', methods=['GET', 'POST'])
def soil():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('soil.html')
    
@app.route('/body_temp', methods=['GET', 'POST'])
def body_temp():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('body_temp.html')

@app.route('/temp', methods=['GET', 'POST'])
def temp():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('temp.html')
    
@app.route('/setting', methods=['GET', 'POST'])
def setting():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('setting.html')

@app.route('/body_move', methods=['GET', 'POST'])
def body_move():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('body_move.html')
    
@app.route('/banner', methods=['GET', 'POST'])
def banner():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('index.html')

@app.route('/dataall', methods=['GET', 'POST'])
def dataall():
    if request.method == 'POST':
        # do stuff when the form is submitted

        # redirect to end the POST handling
        # the redirect can be to the same route or somewhere else
        return redirect(url_for('index'))

    # show the form, it wasn't submitted
    return render_template('data.html')
    
@app.route('/_humidity')
def _humidity():
    global humiditys
    alldata=db.read_humiddata()    
    humiditys=alldata[1]
    return jsonify(humiditystate=humiditys)

@app.route('/_temps')
def _temps():
    global curr_temp
    alldata=db.read_tempdata()    
    curr_temp=alldata[1]
    return jsonify(tempstate=curr_temp)

@app.route('/_vibe')
def _vibe():
    global curr_vibe
    alldata=db.read_vibedata()    
    curr_vibe=alldata[1]
    return jsonify(vibestate=curr_vibe)

@app.route('/_soil')
def _soil():
    global curr_soil
    alldata=db.read_soildata()    
    curr_soil=alldata[1]
    return jsonify(soilstate=curr_soil)    	
    
@app.route('/_all')
def _all():
    tempdata=0
    tempmin=0
    tempmax=0
    strs=''
    alldata=db.read_tempdata() 
    tempdata=alldata
    strs=strs+str(alldata[1])+',' 
    alldata=db.read_humiddata() 
    humiddata=alldata    
    strs=strs+str(alldata[1])+','  
    alldata=db.read_vibedata() 
    vibedata=alldata
    strs=strs+str(alldata[1])+','  
    alldata=db.read_soildata()
    soildata=alldata
    strs=strs+str(alldata[1])+','
    tempmin = db.read_temp_limit_min()
    tempmax = db.read_temp_limit_max()
    if tempdata[1]>= tempmin[1] and tempdata[1]<=tempmax[1]:
		strs=strs+str(0)+','
    else:
		strs=strs+str(1)+','
    tempmin = db.read_vibe_limit_min()
    tempmax = db.read_vibe_limit_max()
    if vibedata[1]>= tempmin[1] and vibedata[1]<=tempmax[1]:
		strs=strs+str(0)+','
		print ("Vibration Value = " +  str(vibedata[1]) )
    else:
		strs=strs+str(1)+','
		msg = "Vibration out of limit = " + str(vibedata[1])
				
    tempmin = db.read_soil_limit_min()
    tempmax = db.read_soil_limit_max()
    if soildata[1]>= tempmin[1] and soildata[1]<=tempmax[1]:
		strs=strs+str(0)+','
    else:
		strs=strs+str(1)+','
		msg = "Soil Moisture out of limit = " + str(vibedata[1])
    tempmin = db.read_humid_limit_min()
    tempmax = db.read_humid_limit_max()
    if humiddata[1]>= tempmin[1] and humiddata[1]<=tempmax[1]:
		strs=strs+str(0)+','
    else:
		strs=strs+str(1)+','
		msg = "Humidity out of limit = " + str(vibedata[1])
		
    return jsonify(allstate=strs)
    
@app.route('/_filterall')
def _filterall():
    global str_filter
    return jsonify(allfilter=str_filter)
    
@app.route('/_allsetting')
def _allsetting():
    alldata=""
    send_em =db.read_send_email()
    alldata = alldata + str(send_em[1][1]) + ","
    limitvalue=db.read_temp_limit_min()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_temp_limit_max()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_humid_limit_min()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_humid_limit_max()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_vibe_limit_min()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_vibe_limit_max()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_soil_limit_min()
    alldata = alldata + str(limitvalue[1]) + ","
    limitvalue=db.read_soil_limit_max()
    alldata = alldata + str(limitvalue[1]) + ","
    return jsonify(allsetting=alldata)
    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
